from django.core.management.base import BaseCommand, CommandError
from rest.models import ShoeType, ShoeColor


class Command(BaseCommand):
    help = "initialize data for ShoeType and ShoeColor"

    def handle(self, *args, **options):
        shoetypes = ['sneaker', 'boot', 'sandal', 'dress', 'other']
        shoecolors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'black', 'white']
        for shoetype in shoetypes:
            ShoeType.objects.create(
                style=shoetype
            )
        for shoecolor in shoecolors:
            ShoeColor.objects.create(
                color_name=shoecolor
            )