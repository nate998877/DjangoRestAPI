from rest_framework import viewsets
from rest.models import *
from rest.serializers import *

class ShoeView(viewsets.ModelViewSet):
    queryset = Shoe.objects.all()
    serializer_class = ShoeSerializer

class ManufacturerView(viewsets.ModelViewSet):
    queryset =  Manufacturer.objects.all()
    serializer_class= ManufacturerSerializer

class ShoeTypeView(viewsets.ModelViewSet):
    queryset = ShoeType.objects.all()
    serializer_class= ShoeTypeSerializer

class ShoeColorView(viewsets.ModelViewSet):
    queryset = ShoeColor.objects.all()
    serializer_class= ShoeColorSerializer
