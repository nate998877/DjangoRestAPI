from django.db import models

class Manufacturer(models.Model):
    name = models.CharField(max_length=100)
    website = models.URLField()

class ShoeType(models.Model):
    style = models.CharField(max_length=30)

class ShoeColor(models.Model):
    color_name = models.CharField(max_length=30)


class Shoe(models.Model):
    size = models.IntegerField()
    brand_name = models.CharField(max_length=30)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.DO_NOTHING)
    color = models.ForeignKey(ShoeColor, on_delete=models.DO_NOTHING)
    material = models.TextField()
    shoe_type = models.ForeignKey(ShoeType, on_delete=models.DO_NOTHING)
    fasten_type = models.CharField(max_length=30)